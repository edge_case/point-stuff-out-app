class CreatePoints < ActiveRecord::Migration[5.2]
  def change
    create_table :points do |t|
      t.float :longitude
      t.float :latitude
      t.boolean :active
      t.text :description
      t.integer :category

      t.timestamps
    end
  end
end
