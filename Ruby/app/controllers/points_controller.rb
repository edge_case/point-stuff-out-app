class PointsController < ApplicationController
    protect_from_forgery with: :null_session
    
    def create
        @point = Point.new
        @point.longitude = params.require(:long)
        @point.latitude = params.require(:lat)
        @point.active = true
        @point.description = params.require(:description)
        puts params[:category]
        unless params[:category].nil?
            @point.category  = params[:category]
        end
        @point.save
    end

    def index
        puts params[:filter]
        @points = Point.all
        render :json => @points
    end

    def show
        @point = Point.find(params[:id])
        render :json => @point
    end

    # Ignore OPTIONS request, but send positive status code
    def options
        head 200
    end

    # Set JSON as default format
    private def set_default_format
        request.format = :json
    end
end
